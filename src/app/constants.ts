export class Constants {
  public static readonly EMAIL_REGEXP: RegExp = /^(?=.{0,255}$)([a-zA-Z](\.?[a-zA-Z0-9_+-])*@[a-zA-Z0-9-]+\.([a-zA-Z]{1,6}\.)?[a-zA-Z]{2,15}$)/;
  public static readonly PASSWORD_REGEXP: RegExp = /^(?=.*\d)(?=.*[a-z|а-я])(?=.*[A-Z|А-Я]).{6,32}$/;
}
