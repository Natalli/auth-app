import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {Password} from '../../../components/model/password.model';
import {PasswordService} from '../../../components/services/password.service';

@Component({
    selector: 'app-add-password',
    templateUrl: './add-password.component.html',
    styleUrls: ['./add-password.component.css']
})
export class AddPasswordComponent implements OnInit {
    public addForm: FormGroup;
    passwords: Password[];
    isVisible: boolean;
    user: any;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private passwordService: PasswordService) {
    }

    ngOnInit() {
        this.passwordService
            .getPassword()
            .subscribe((data: Password[]) => {
                this.passwords = data;
            });
        this.user = JSON.parse(window.localStorage.getItem('user'));
        this.createAddForm();
    }

    createAddForm() {
        this.addForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.required])],
            login: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])],
        });
    }

    add() {
        const data = {
            ownerId: this.user.id,
            name: this.addForm.value.name,
            login: this.addForm.value.login,
            pass: this.addForm.value.password
        };
        this.passwordService
            .addPassword(data)
            .subscribe((pass: Password) => {
                console.log(this.passwords);
                this.passwords.push(pass);
                console.log(this.passwords);
            });
        this.router.navigate(['/']);
    }

}
