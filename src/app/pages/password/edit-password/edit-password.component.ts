import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Router, ActivatedRoute} from '@angular/router';
import {PasswordService} from '../../../components/services/password.service';
import {Password} from '../../../components/model/password.model';


@Component({
    selector: 'app-edit',
    templateUrl: './edit-password.component.html',
    styleUrls: ['./edit-password.component.css']
})
export class EditPasswordComponent implements OnInit {
    id: number;
    sub: any;
    password: Password;
    isVisible: boolean;
    public editForm: FormGroup;
    passwords: Password[];
    isLoaded: boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private passwordService: PasswordService) {
    }

    ngOnInit() {
        this.passwordService
            .getPassword()
            .subscribe((data: Password[]) => {
                this.passwords = data;
                this.sub = this.route.params
                    .subscribe(params => {
                        this.id = +params['id'];
                        this.password = this.passwords.find(item => {
                            return item.id === this.id;
                        });
                        console.log(this.password);
                        this.isLoaded = true;
                    });
            });
        // this.createAddForm();
    }

    createAddForm() {
        this.editForm = this.formBuilder.group({
            name: [this.password.name, Validators.compose([Validators.required])],
            // login: ['', Validators.compose([Validators.required])],
            // password: ['', Validators.compose([Validators.required])]
        });
    }

    edit() {
        const data = {
            name: this.password.name,
            pass: this.password.pass
        };
        this.passwordService
            .editPassword(this.password, data)
            .subscribe((i) => {
                console.log(i);
            });
        this.router.navigate(['/']);
    }


}
