import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {Constants} from '../../../constants';
import {UserService} from '../../../components/services/user.service';
import {User} from '../../../components/model/user.model';
import {Alert} from '../../../components/model/alert.model';
import {AuthService} from '../../../components/services/auth.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;
    isVisible: boolean;
    message: Alert;

    constructor(private formBuilder: FormBuilder,
                private userService: UserService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.message = new Alert('danger', '');
        this.route.queryParams
            .subscribe((params: Params) => {
                console.log(params);
                if (params['nowCanLogin']) {
                    this.showMessage({
                        text: 'You can login in system now!',
                        type: 'success'
                    });
                } else if (params['accessDenied']) {
                    this.showMessage({
                        text: 'To work in the system you need to log in',
                        type: 'warning'
                    });
                }
            });
        this.createLoginForm();
    }

    private showMessage(message: Alert) {
        this.message = message;
        window.setTimeout(() => {
            this.message.text = '';
        }, 5000);
    }

    createLoginForm() {
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required, Validators.pattern(Constants.EMAIL_REGEXP)])],
            password: ['', Validators.compose([Validators.required])],
        });
    }

    login() {
        const formData = this.loginForm.value;

        this.userService.getUserByEmail(formData.email)
            .subscribe((user: User) => {
                if (user) {
                    if (user.password === formData.password) {
                        this.message.text = '';
                        window.localStorage.setItem('user', JSON.stringify(user));
                        this.authService.login();
                        this.router.navigate(['/']);
                    } else {
                        this.showMessage({
                            text: 'Password is wrong!',
                            type: 'danger'
                        });
                    }
                } else {
                    this.showMessage({text: 'User not found!', type: 'danger'});
                }
            });
    }

}
