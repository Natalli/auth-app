import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {UserService} from '../../../components/services/user.service';
import {Constants} from '../../../constants';
import {User} from '../../../components/model/user.model';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    public registerForm: FormGroup;
    isVisible = false;
    isConfirmVisible = false;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService,
        private router: Router) {
    }

    ngOnInit() {
        this.createRegisterForm();
    }


    createRegisterForm() {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required, Validators.pattern(Constants.EMAIL_REGEXP)]),
                this.forbiddenEmails.bind(this)],
            password: ['', Validators.compose([Validators.required, Validators.pattern(Constants.PASSWORD_REGEXP)])],
            passwordConfirm: ['', Validators.compose([Validators.required, Validators.pattern(Constants.PASSWORD_REGEXP)])],
        });
    }

    register() {
        const {name, email, password} = this.registerForm.value;
        const user = new User(email, password, name);
        console.log(user);
        if (this.registerForm.valid) {
            this.userService
                .createNewUser(user)
                .subscribe(() => {
                    this.router.navigate(['/login'], {
                        queryParams: {
                            nowCanLogin: true
                        }
                    });
                });
        }
    }

    forbiddenEmails(control: FormControl): Promise<any> {
        console.log(control);
        return new Promise((resolve, reject) => {
            this.userService.getUserByEmail(control.value)
                .subscribe((user: User) => {
                    console.log(user);
                    if (user) {
                        resolve({forbiddenEmails: true});
                        // return user ? resolve({forbiddenEmails: true}) : resolve(null);
                    } else {
                        resolve(null);
                    }
                });
        });
    }
}
