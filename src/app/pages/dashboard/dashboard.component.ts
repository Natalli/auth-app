import {Component, OnInit} from '@angular/core';

import {PasswordService} from '../../components/services/password.service';
import {Password} from '../../components/model/password.model';
import {User} from '../../components/model/user.model';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    isVisible: Array<boolean> = [];
    passwords: Password[] = [];
    user: User;
    currentPass: any;

    constructor(
        private passwordService: PasswordService) {
    }

    ngOnInit() {
        this.user = JSON.parse(window.localStorage.getItem('user'));
        this.passwordService
            .getPassword()
            .subscribe((passwords: Password[]) => {
                this.passwords = passwords;
                if (this.user) {
                    this.currentPass = this.passwords.filter(item => {
                        return this.user.id === item.ownerId;
                    });
                }
            });
    }

    onRemove(id) {
        this.passwordService
            .deletePassword(id)
            .subscribe(() => {
                this.passwords = this.passwords.filter((data) => data.id !== id);
            });
    }
}
