export class Password {
    constructor(
        public ownerId: number,
        public name: string,
        public login: string,
        public pass: string,
        public id?: number) {
    }

}
