import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {User} from '../model/user.model';

@Injectable()
export class UserService {
    constructor(private router: Router, private http: HttpClient) {

    }

    getUserByEmail(email: string): Observable<User> {
        return this.http.get(`http://localhost:3000/users?email=${email}`)
            .pipe(map((response: any) => response))
            .pipe(map((user: User[]) => user[0] ? user[0] : undefined));
    }

    createNewUser(user: User): Observable<User> {
        return this.http.post('http://localhost:3000/users', user)
            .pipe(map((response: any) => response));
    }

    logout() {}
}
