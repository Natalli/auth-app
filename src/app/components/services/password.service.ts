import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Password} from '../model/password.model';

@Injectable()
export class PasswordService {

    constructor(
        private http: HttpClient) {
    }

    getPassword() {
        return this.http.get('http://localhost:3000/passwords');
    }

    addPassword(data) {
        return this.http.post('http://localhost:3000/passwords', data);
    }

    editPassword(password: Password, data) {
        password.name = data.name;
        password.pass = data.pass;
        return this.http.put(`http://localhost:3000/passwords/${password.id}`, password);
    }

    deletePassword(id: number) {
       return this.http.delete(`http://localhost:3000/passwords/${id}`);
    }
}
