import {Component, OnInit} from '@angular/core';

import {User} from '../model/user.model';
import {UserService} from '../services/user.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    user: User;

    constructor(
        private userService: UserService
    ) {
    }

    ngOnInit() {
        this.user = JSON.parse(window.localStorage.getItem('user'));
    }

    logOut() {
        this.userService.logout();
    }
}
