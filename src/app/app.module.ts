import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {LoginComponent} from './pages/auth/login/login.component';
import {RegisterComponent} from './pages/auth/register/register.component';
import {CentralViewComponent} from './components/central-view/central-view.component';
import {UserService} from './components/services/user.service';
import {AddPasswordComponent} from './pages/password/add-password/add-password.component';
import {PasswordService} from './components/services/password.service';
import {EditPasswordComponent} from './pages/password/edit-password/edit-password.component';
import {AuthService} from './components/services/auth.service';
import {AuthGuard} from './components/services/auth.guard';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        LoginComponent,
        RegisterComponent,
        CentralViewComponent,
        AddPasswordComponent,
        EditPasswordComponent,
        HeaderComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [UserService, AuthService, AuthGuard, PasswordService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
