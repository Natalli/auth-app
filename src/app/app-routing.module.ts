import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {LoginComponent} from './pages/auth/login/login.component';
import {RegisterComponent} from './pages/auth/register/register.component';
import {CentralViewComponent} from './components/central-view/central-view.component';
import {AddPasswordComponent} from './pages/password/add-password/add-password.component';
import {EditPasswordComponent} from './pages/password/edit-password/edit-password.component';
import {AuthGuard} from './components/services/auth.guard';

const routes: Routes = [
    {
        path: '', component: CentralViewComponent,
        children: [
            {path: 'login', component: LoginComponent},
            {path: 'register', component: RegisterComponent},
            {path: '', component: DashboardComponent},
            {path: 'add', component: AddPasswordComponent},
            {path: 'edit-password/:id', component: EditPasswordComponent}
        ]
    },

    {path: '', redirectTo: '/', pathMatch: 'full'}];


@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes,
        {
            useHash: true,
            scrollPositionRestoration: 'enabled'
        })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
